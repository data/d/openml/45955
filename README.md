# OpenML dataset: Credit_Card_Fraud_

https://www.openml.org/d/45955

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Dataset Name**: card_transdata.csv

**Description**:
This dataset captures transaction patterns and behaviors that could indicate potential fraud in card transactions. The data is composed of several features designed to reflect the transactional context such as geographical location, transaction medium, and spending behavior relative to the user's history.

**Attribute Description**:
1. **distance_from_home**: This is a numerical feature representing the geographical distance in kilometers between the transaction location and the cardholder's home address.
2. **distance_from_last_transaction**: This numerical attribute measures the distance in kilometers from the location of the last transaction to the current transaction location.
3. **ratio_to_median_purchase_price**: A numeric ratio that compares the transaction's price to the median purchase price of the user's transaction history.
4. **repeat_retailer**: A binary attribute where '1' signifies that the transaction was conducted at a retailer previously used by the cardholder, and '0' indicates a new retailer.
5. **used_chip**: This binary feature indicates whether the transaction was made using a chip (1) or not (0).
6. **used_pin_number**: Another binary feature, where '1' signifies the use of a PIN number for the transaction, and '0' shows no PIN number was used.
7. **online_order**: This attribute identifies whether the purchase was made online ('1') or offline ('0').
8. **fraud**: A binary target variable indicating whether the transaction was fraudulent ('1') or not ('0').

**Use Case**:
This dataset is particularly suited for developing machine learning models to detect potentially fraudulent transactions. Financial institutions and cybersecurity firms can leverage this data to enhance their fraud detection systems, ensuring safer transaction environments for cardholders. Researchers in fintech and cybersecurity can also use this dataset for academic purposes, exploring new methodologies in fraud detection algorithms. Additionally, policy makers and regulatory bodies can analyze trends and patterns to formulate guidelines that mitigate transactional fraud.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45955) of an [OpenML dataset](https://www.openml.org/d/45955). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45955/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45955/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45955/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

